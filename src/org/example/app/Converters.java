package org.example.app;

import java.util.HashMap;
import java.util.Map;

public class Converters {

    private Boolean isRomanNumber = false;
    private int[] arabianNumbersArray = {1, 4, 5, 9, 10, 40, 50, 90, 100};
    private String[] romanNumbersArray = {"I", "IV", "V", "IX", "X", "XL", "L", "XC", "C"};

    public Integer[] fromStringToInteger(String a, String b) {
        Integer[] integerArray = new Integer[2];
        RomanValidator romanValidator = new RomanValidator();

        if (romanValidator.checkIfRomanNumber(a) && romanValidator.checkIfRomanNumber(b)) {
            isRomanNumber = true;
            integerArray[0] = fromRomanToInt(a);
            integerArray[1] = fromRomanToInt(b);
        } else {
            try {
                integerArray[0] = Integer.valueOf(a);
                integerArray[1] = Integer.valueOf(b);
            } catch (NumberFormatException e) {
                throw new NumberFormatException("Складывать римские и арабские числа - такое себе");
            }
        }

        return integerArray;
    }

    public String fromIntegerToString(Integer value) {
        if (isRomanNumber) {
            if (value <= 0) {
                throw new RuntimeException("Римские числа не могут равняться нулю или быть отрицательными");
            }
            return fromIntegerToRoman(value);
        }

        return String.valueOf(value);
    }

    public String fromIntegerToRoman(Integer number) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int el = arabianNumbersArray.length - 1; el >= 0 && number > 0; el--) {
            while (number >= arabianNumbersArray[el]) {
                number -= arabianNumbersArray[el];
                stringBuilder.append(romanNumbersArray[el]);
            }
        }
        return stringBuilder.toString();
    }

    public int fromRomanToInt(String string) {
        Map<Character, Integer> map = new HashMap<>();
        map.put('I', 1);
        map.put('V', 5);
        map.put('X', 10);

        int result = map.get(string.charAt(string.length() - 1));

        for (int el = string.length() - 2; el >= 0; el--) {
            if (map.get(string.charAt(el)) < map.get(string.charAt(el + 1))) {
                result -= map.get(string.charAt(el));
            } else {
                result += map.get(string.charAt(el));
            }
        }
        return result;
    }


}
