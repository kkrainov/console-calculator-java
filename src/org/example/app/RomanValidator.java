package org.example.app;

public class RomanValidator {
    public Boolean checkIfRomanNumber(String string) {
        return (string.contains("I") || string.contains("V") || string.contains("X"));
    }
}
