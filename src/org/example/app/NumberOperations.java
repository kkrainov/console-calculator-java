package org.example.app;

import java.util.Arrays;
import java.util.Scanner;

public class NumberOperations {

    private String a;
    private String sign;
    private String b;

    public String getA() {
        return a;
    }

    public String getSign() {
        return sign;
    }

    public String getB() {
        return b;
    }

    public void handleInput() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Пожалуйста, введите своё выражение арабскими числами от 1 до 10 или используйте римские числа от I до X. Затем нажмите Enter. Примеры выражений: 1 + 10 или V + IV ");


        String[] handledData = scanner.nextLine().toUpperCase().split(" ");

        a = handledData[0];

        sign = handledData[1];

        b = handledData[2];

        if (handledData.length < 3) {
            System.out.println("Слишком коротко. Попробуйте ещё раз");
        } else {
            System.out.println("Данные получены. Спасибо!");
        }
    }

    public String calculate(String a, String sign, String b) {
        Converters converter = new Converters();
        Integer[] integerValues = converter.fromStringToInteger(a, b);
        Integer integerValueA = integerValues[0];
        Integer integerValueB = integerValues[1];

        if (integerValueA > 10 || integerValueB > 10) {
            throw new RuntimeException("Числа больше 10 не поддерживаются");
        }

        try {
            switch (sign) {
                case "+":
                    return converter.fromIntegerToString(integerValueA + integerValueB);
                case "-":
                    return converter.fromIntegerToString(integerValueA - integerValueB);
                case "*":
                    return converter.fromIntegerToString(integerValueA * integerValueB);
                case "/":
                    return converter.fromIntegerToString(integerValueA / integerValueB);
                default:
                    throw new RuntimeException("Данная операция не поддерживается");
            }
        }
        catch (ArithmeticException ex) {
            throw new ArithmeticException("На ноль обычно не делится");
        }
    }

}
