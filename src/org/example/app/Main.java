package org.example.app;

import java.util.Arrays;
import java.util.Scanner;
import org.example.app.NumberOperations;

public class Main {

    public static void main(String[] args) {
        NumberOperations operation = new NumberOperations();

        while(true) {
            operation.handleInput();
            String result = new String();

            result = operation.calculate(operation.getA(), operation.getSign(), operation.getB());

            System.out.println(result);
        }
    }

}
